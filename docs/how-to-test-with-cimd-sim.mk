# How to test the appstore USSD with CIMD Simulator #

## Prerequisites ##

    You need the following projects cloned/checked-out
        (i)     Iluka - middleware/iluka.git
        (ii)    Appstore USSD - appstore/appstore-ussd.git (obviously)
        (iii)   USSD Connector - hsenid-component/ussd-connector.git
        (iv)    CIMD Connector and Simulator - http://192.168.0.7/svn/hms-commons/connectors/sbl/cimd/

## The Steps ##

    (i) Build the Iluka
            a) Set the iluka.home in <ILUKA_PROJECT_HOME>/pom.xml
            b) then, mvn clean install

    (ii) Build Appstore USSD
            a) edit. <APPSTORE_USSD_HOME>/core/pom.xml
            b) Find the following dependency

                    <dependency>
                        <groupId>hsenidmobile.iluka</groupId>
                        <artifactId>test-kit</artifactId>
                        <version>1.0</version>
                        <scope>test</scope>
                    </dependency>

            c) remove <scope>test</scope> from above dependency
            d) Edit the following file,
                <APPSTORE_USSD_HOME>/core/src/main/resources/context.dsl
            e) In this file, change the following property

                app.connector.class.name="hsenidmobile.iluka.connectors.http.handler.SdpIlukaConnector"

                to

                app.connector.class.name="hsenidmobile.iluka.connectors.cimd.CimdConnector"


                [Please Note that once the testing is done you must revert value of app.connector.class.name
                 back to SdpIlukaConnector]

            f) Extra Note:

                 if app.connector.class.name is  "...SdpIlukaConnector" then
                    sdp-ussd-connector.properties will be used by the appsotre
                 else if app.connector.class.name is  "...CimdConnector" then
                    cimd.properties

            g) Edit the cimd.properties

                ussd.host=192.168.0.234
                ussd.port=6100


                ussd.host server url of CIMD Simulator. ussd.port is the port where CIMD Simulator up and running.
                this port should be the one that is defined in ussdc.properties in CIMD Simulator.

            f) Build the appstore ussd
                    mvn clean install
                    OUTPUT: target/appstore-ussd-2.0.tar.gz

    (iii) Cimd Simulator

            a) Find and edit <CIMD_HOME>/simulator/src/main/resources/ussdc.properties
                - The port you set to ussd.port property must reflect in (ii)g
                - jetty.host=localhost //CIMD Simulator Web Interface IP
                - jetty.port=8080      //CIMD Simulator Web Interface PORT

            b) build the project from <CIMD_HOME>

                mvn clean install

            c) goto <CIMD_HOME>/simulator/target/cimdsimulator/bin

                        Start CIMD Simulator:        ./cimdsimulator start
                        Stop CIMD Simulator:         ./cimdsimulator stop

            d) CIMD Simulator Web URL:

                http://${jetty.host}.${jetty.port}/

                ex:
                    http://localhost:8080/ussdsim


    (iv) Deploying Appstore USSD in Iluka Server

            a) Find the iluka distributor in <ILUKA_PROJECT_HOME>/distrib/target/iluka
                                                            (Please Note: this is called <ILUKA_SERVER> here)

            b) untar appstore-ussd-2.0.tar.gz and deploy it in the following location
                    <ILUKA_SERVER>/apps

            c) Start the iluka
                    cd <ILUKA_SERVER>/bin
                    ./iluka start

    (v) Now goto CIMD Simulator UI (Refer "(iii)d" for the URL) and test Appstore USSD Bad Boy


    (vi) And As Always Enjoy!! :)


## END ##