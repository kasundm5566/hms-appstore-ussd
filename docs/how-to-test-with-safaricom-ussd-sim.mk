# How to test the appstore USSD with Safaricom-Ussd Simulator #

## Prerequisites ##

    You need the following projects cloned/checked-out
        (i)     Iluka - middleware/iluka.git
        (ii)    Appstore USSD - appstore/appstore-ussd.git
        (iii)   SDP - messaging/sdp.git for Safaricom-Ussd Simulator

## The Steps ##

    (i) Import Initial Data
            Important :
                a) You need to have a database named as kite and relevant collections as mentioned in SDP.
                b) You should add/edit create-app.json and create-ncs_sla.json in data/mongo folder with ip address which
                   you are going to deploy your application.
                    'allowed-hosts' in create-app.json
                    'connection-url' in create-ncs_sla.json

            Execute the following commands in order to have a 'Safaricom Application' which use ncs_type 'USSD'
            in SDP. You can edit initial data in the /data/mongo folder.
                mongoimport --db kite --collection sp --file <project-folder>/data/mongo/create-sp.json
                mongoimport --db kite --collection app --file <project-folder>/data/mongo/create-app.json
                mongoimport --db kite --collection routing_keys --file <project-folder>/data/mongo/create-routing_keys.json
                mongoimport --db kite --collection ncs_sla --file <project-folder>/data/mongo/create-ncs_sla.json

    (ii) Build the Iluka
            a) Build the project using "mvn clean install" from the base directory

            b) Extra Note:

                - For further details look into the README file in Iluka project
                - Iluka should be build before building the Appstore USSD project

    (iii) Build Appstore USSD
            a) Edit the following files,

                (i)<APPSTORE_USSD_HOME>/core/src/main/resources/sdp-ussd-connector.properties

                    Following properties need to be set
                        app.id - appID of above safaricom ussd app which you import as initial data
                        app.password - password of the app
                        ussd.listener.host - location where Appstore USSD is going to deployed
                        ussd.listener.port - listener port for the 'ussd.listener.host'
                        ussd.sender.host - location of sdp using for the testing
                        ussd.sender.port - sender port for the 'ussd.sender.host'
                        ussd.sender.path - sender path for the 'ussd.sender.host'

                    Ex:
                        app.id=APP_000200
                        app.password=79e0351c122eaf27c97e63e41fbcb320
                        ussd.listener.host=0.0.0.0
                        ussd.listener.port=8090
                        ussd.sender.host=sdp.vck
                        ussd.sender.port=7000
                        ussd.sender.path=/ussd/send

                (ii)<APPSTORE_USSD_HOME>/core/src/main/resources/appstore-ussd.properties

                    Set the following properties or set proper DNS in /etc/hosts
                        store.app.vck - appstore web module location
                        cur.vck - common user registration location
                        sdp.vck - sdp location

            b) Build the appstore ussd
                    mvn clean install
                    OUTPUT: /core/target/appstore-ussd-2.0.tar.gz

    (iv) Deploying Appstore USSD in Iluka Server

            a) Find the iluka distributor in <ILUKA_PROJECT_HOME>/distrib/target/iluka
                                                            (Please Note: this is called <ILUKA_SERVER> here)

            b) untar appstore-ussd-2.0.tar.gz and deploy it in the following location
                    <ILUKA_SERVER>/apps

            c) Start the iluka
                    cd <ILUKA_SERVER>/bin
                    ./iluka start

    (v) Safaricom-ussd simulator

            a) Go to the /simulators/safaricom-ussd in SDP project and build using
	            mvn clean assembly:assembly
            b) Go to the target folder and extract safaricom-ussd-sim-1.0-SNAPSHOT-bin.tar
            c) Go to extracted folder and execute
                  java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://sbl.sdp.vck:<safaricom.listner.http.port in sbl>/ ^#[0-9]+#[0-9#]*$
                      Use given regx to validate first ussd request
                        OR
                  java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://sbl.sdp.vck:<safaricom.listner.http.port in sbl>/

                  Ex:
                  java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://192.168.0.195:7821/ ^#[0-9]+#[0-9#]*$
                  java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://192.168.0.195:7821/

## END ##