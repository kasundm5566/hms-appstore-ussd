package hms.appstore.ussd

import hsenidmobile.iluka.test.AbstractIntegrationTest
import hsenidmobile.iluka.iapi.Message
import hsenidmobile.iluka.connectors.mock._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
abstract class VishmaIntegrationTest extends AbstractIntegrationTest {

  def testMainMenu() {
    val menuFlow = List("*383#", "6", "1234", "6", "1" , "1")
    val msisdn = "254717896321"

    val results = for {
      keyIn <- menuFlow
    } yield (execInput(msisdn, keyIn))

    println(results.mkString("\n"))
  }

  def execInput(user: String, input: String): String = {
    MessageProxy ! Message(msisdn = user, data = input)
    MessageProxy.receiveMt.data
  }
}


