resource.bundle.basename={
    description="Iluka resource bundle file base name";
    type="string";
    values=[
        default="messages",
        test="messages",
        dev="messages",
        production="messages"
    ];
}

default.language={
    description="Iluka default language";
    type="string";
    values=[
        default="en",
        test="en",
        dev="en",
        production="en"
    ];
}

locale.dcs.mapping={
    description="Locale to DCS mapping";
    type="string";
    values=[
        default="en -> 0; zh -> 72; ms -> 0",
        test="en -> 0; zh -> 72; ms -> 0",
        dev="en -> 0; zh -> 72; ms -> 0",
        production="en -> 0; zh -> 72; ms -> 0"
    ];
}

ussd.message.prefix={
    description="Prefix of ussd service request, It should be provided as Regex";
    type="string";
    values=[
        default="[\\*|\\#]",
        test="[\\*|\\#]",
        dev="[\\*|\\#]",
        production="[\\*|\\#]"
    ];
}

ussd.message.suffix={
    description="Suffix of ussd service request, It should be provided as Regex";
    type="string";
    values=[
        default="[\\#]",
        test="[\\#]",
        dev="[\\#]",
        production="[\\#]"
    ];
}

ussd.message.separator={
    description="Separator of ussd service request, It should be provided as Regex";
    type="string";
    values=[
        default="[\\*]",
        test="[\\*]",
        dev="[\\*]",
        production="[\\*]"
    ];
}

ussd.session.timeout={
    description="Ussd session timeout";
    type="int";
    values=[
        default="40000",
        test="40000",
        dev="40000",
        production="40000"
    ];
}

stale.session.cleanup.period={
    description="Obsolete session cleanup period in milli seconds";
    type="int";
    values=[
        default="900000",
        test="900000",
        dev="900000",
        production="900000"
    ];
}