ActionMapping {
    mainPaging="mainPagingAction"
    searchApps="searchAppsAction"
    listCategories= "listCategoriesAction"
    listFeaturedApps= "listFeaturedAppsAction"
    viewSearchResult= "viewSearchResultAction"
    listApps= "listAppsAction"
    listMyApps = "listMyAppsAction"
    viewAppDetails= "viewAppDetailsAction"
    viewFeaturedAppDetails= "viewFeaturedAppDetailsAction"
    viewMyAppDetails= "viewMyAppDetailsAction"
    login = "loginAction"
    changeMpin = "changeMpinAction"
    enterNewMpin = "enterNewMpinAction"
    confirmChangeMpin = "confirmChangeMpinAction"
    enterMpin = "enterMpinAction"
    confirmMpin = "confirmMpinAction"
    chargeInfo = "appChargeInfoAction"
    appInfo = "appInfoAction"
    subscribe = "subscribeAction"
    unSubscribe = "unSubscribeAction"
    loginRedirect = "loginRedirectAction"
    download = "downloadAction"
}

/**
 Menu Filters
**/

hms.appstore.ussd.filter.AuthRule authRule {
}

hms.appstore.ussd.filter.LoginRule loginRule {

}

hms.appstore.ussd.filter.SubExclusionRule subscriptionRule {

}

hms.appstore.ussd.filter.UnSubExclusionRule unSubscriptionRule {

}

hms.appstore.ussd.filter.DownloadRule downloadRule {

}

/**
 Paging Actions
 **/

hms.appstore.ussd.menu.MainMenuAction mainPagingAction {
  itemsPerPage = 7
}

hms.appstore.ussd.menu.SearchAppsAction searchAppsAction {
  emptyPageMessageKey = "no.result.found"
}

hms.appstore.ussd.menu.ListFeaturedAppsAction listFeaturedAppsAction {

}

hms.appstore.ussd.menu.ListAppsAction listAppsAction {

}

hms.appstore.ussd.menu.ListMyAppsAction listMyAppsAction {
  emptyPageMessageKey = "no.subscribed.apps"
}

hms.appstore.ussd.menu.ListCategoriesAction listCategoriesAction {

}

hms.appstore.ussd.menu.ViewAppDetailsAction viewSearchResultAction {
    parentMenuName = "searchResultsMenu"
}

hms.appstore.ussd.menu.ViewAppDetailsAction viewAppDetailsAction {
    parentMenuName = "appListMenu"
}

hms.appstore.ussd.menu.ViewAppDetailsAction viewFeaturedAppDetailsAction {
    parentMenuName = "featuredAppListMenu"
}

hms.appstore.ussd.menu.ViewAppDetailsAction viewMyAppDetailsAction {
    parentMenuName = "myAppsListMenu"
}

/**
 All action back end calls
 **/

hms.appstore.ussd.action.LoginRedirectAction loginRedirectAction {

}

hms.appstore.ussd.action.SubscribeAppAction subscribeAction {

}

hms.appstore.ussd.action.UnsubscribeAppAction   unSubscribeAction {

}

hms.appstore.ussd.action.LoginAction loginAction {
 formInputs = ["m-pin"]
}

hms.appstore.ussd.action.ChangeMpinAction changeMpinAction {
 formInputs = ["old-m-pin"]
}

hms.appstore.ussd.action.EnterNewMpinAction enterNewMpinAction {
 formInputs = ["entered-new-m-pin"]
}

hms.appstore.ussd.action.ConfirmChangeMpinAction confirmChangeMpinAction {
 formInputs = ["confirm-new-m-pin"]
}

hms.appstore.ussd.action.EnterMpinAction enterMpinAction {
 formInputs = ["entered-m-pin"]
}

hms.appstore.ussd.action.ConfirmMpinAction confirmMpinAction {
 formInputs = ["confirm-m-pin"]
}

hms.appstore.ussd.action.DownloadAppAction downloadAction {

}

hms.appstore.ussd.action.AppInfoAction appInfoAction {

}

hms.appstore.ussd.action.AppChargeInfoAction appChargeInfoAction {

}

