/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.appstore.ussd

import hsenidmobile.iluka.iapi.utils.Log4s
import hsenidmobile.iluka.iapi.IlukaApp

class AppStoreUssd(appHome: String) extends IlukaApp with Log4s {

  override def start() {
    debug("Starting the appstore-ussd 2.1 app ...")
    init()
    super.start()
    debug("appstore-ussd 2.1 has started!")
  }

  def init() {
    preCoreFilters = List()
    postCoreFilters = List()
  }
}