package hms.appstore.ussd.service

import scala.concurrent.duration._

trait UssdConfig {

  /**
   * The max amount of time we can wait for a discovery-api call.
   * Note: This value should be > spray.can.client.request-timeout in application.conf
   * @return timeout duration
   */
  def discoveryApiTimeOut: Duration = 100 seconds

  def numberOfResultPerPage: Int = 10
}
