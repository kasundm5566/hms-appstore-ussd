/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.ussd.service

import hms.appstore.api.client.ClientModule

import com.escalatesoft.subcut.inject.NewBindingModule

object ServiceModule extends NewBindingModule({
  implicit module =>

    module <~ ClientModule
})
