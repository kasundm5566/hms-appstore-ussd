/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.appstore.ussd.action

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s

import hms.appstore.api.json.Application
import hms.appstore.ussd.util.{UssdUtil, SessionExtensions}
import hsenidmobile.iluka.iapi.menulet.Action

import scala.math._
import scalaz._
import Scalaz._


class AppInfoAction extends Action with Log4s {

  def execute(request: Request): Either[HandlerError, Result] = {
    import SessionExtensions._
    import request._

    session.getApp.cata(
      app => {
        def buildMessageString(app: Application) = {

          val operatorId = UssdUtil.getOperatorId(session.msisdn)

          List(app.displayName, " ", "*" * floor(app.rating).toInt, "\n",
            "App By " + app.developer, "\n",
            app.instructions.get(operatorId).getOrElse("No details available!"), "\n",
            appContext.localize("back.menu.key.text")).mkString
        }
        #|>(buildMessageString(app))
      },
      #<>(ErrorCode.ERR501, "Illegal state, missing page paramters")
    )
  }

}
