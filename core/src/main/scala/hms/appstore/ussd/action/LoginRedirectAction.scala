package hms.appstore.ussd.action


import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s

import hms.appstore.api.client.DiscoveryInternalService
import hms.appstore.ussd.service.{ServiceModule, UssdConfig}

import com.escalatesoft.subcut.inject.Injectable
import scala.concurrent.{Await, ExecutionContext}

class LoginRedirectAction extends Action with Log4s with Injectable with UssdConfig {

  val bindingModule = ServiceModule
  private val discoveryInternalService = inject[DiscoveryInternalService]
  private implicit val executionContext = inject[ExecutionContext]

  def execute(request: Request): Either[HandlerError, Result] = {
    import request._
    import msg.msisdn

    val inputParams = Map("msisdn" -> msisdn)
    val mobileNo = inputParams.getOrElse("msisdn", "")

    val loginRedirectFuture = discoveryInternalService.userDetailsByMsisdn(mobileNo)
    val loginRedirectResp = Await.result(loginRedirectFuture, discoveryApiTimeOut)

    if(loginRedirectResp.isSuccess) {
        if(loginRedirectResp.getMPin.isDefined && (!loginRedirectResp.getMPin.get.equals(""))) {
          debug("User has a mpin")
          #>(nexActionName = "loginMenu")
        } else {
          debug("User does not have a mpin")
          #>(nexActionName = "enterMpinMenu")
        }
    } else {
      #>(nexActionName = "notRegisteredMenu")
    }
  }
}