package hms.appstore.ussd.action

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s

import scalaz._
import Scalaz._

import scala.beans.BeanProperty

class EnterNewMpinAction extends Validator with Log4s {
  @BeanProperty var maxRetryAttempts = 3

  override def execute(flowId: String, request: Request) = {
    import request._
    import msg.data

    def validMpin: Boolean = {
      try {
        val mpin = data.toInt
        if((999 < mpin) && (mpin < 10000)) {
          info("Entered Mpin is valid")
          true
        } else {
          info("Entered Mpin is not valid")
          false
        }
      } catch {
        case e: Exception =>
          debug("Entered Mpin is not an integer")
          false
      }
    }

    def retry(i: Int) = {
      if (i >= maxRetryAttempts) {
        #|(appContext.localize("mpin.enter.retry.attempts.exceeded"))
      } else {
        session.+=("enter.mpin.retry.count", i + 1)
        #>(nexActionName = "enterMpinRetryMenu")
      }
    }

    def goToConfirmMpinMenu: Either[HandlerError, Result] = {
      session.+=("enter.mpin.retry.count", 0)
      some(formInputAsMap(data)) map {session.+=(flowId, _)}
      #>(nexActionName = "confirmChangeMpinMenu")
    }

    if (validMpin) goToConfirmMpinMenu
    else session[Int]("enter.mpin.retry.count").cata(retry, retry(0))
  }
}