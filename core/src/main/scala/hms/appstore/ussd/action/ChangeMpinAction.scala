package hms.appstore.ussd.action

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s

import hms.appstore.ussd.util.SessionExtensions._
import hms.appstore.api.client.DiscoveryService
import hms.appstore.ussd.service.{ServiceModule, UssdConfig}

import scalaz._
import Scalaz._

import com.escalatesoft.subcut.inject.Injectable
import scala.concurrent.{Await, ExecutionContext}
import beans.BeanProperty
import hms.appstore.api.json.AuthenticateByMPinReq

class ChangeMpinAction extends Validator with Injectable with UssdConfig with Log4s{
  @BeanProperty var maxRetryAttempts = 2
  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  override def execute(flowId: String, request: Request) = {
    import request._
    import msg.{data, msisdn}
    val inputParams = formInputAsMap(data).map {
      case (k, v) => (k, v)
    }
    val oldMpin = inputParams.getOrElse("old-m-pin", "")
    val futureResult = discoveryService.authenticateByMpin(AuthenticateByMPinReq(msisdn,oldMpin))
    val authResponse = Await.result(futureResult, discoveryApiTimeOut)


    def retry(i: Int) = {
      if (i > maxRetryAttempts) {
        #|(appContext.localize("login.retry.attempts.failed"))
      } else {
        session.+=("login.retry.count", i + 1)
        #>(nexActionName = "validateRetryMenu")
      }
    }

    def goToEnterNewMenu: Either[HandlerError, Result] = {
      session.+=("user.authenticated", true)
      session.+=("login.retry.count", 0)
      #>(nexActionName = "enterNewMpinMenu")
    }

    authResponse.statusCode match {
      case "S1000" =>
        session.setAuthSessionId(authResponse.sessionId.getOrElse(""))
        session.+=("old.m.pin", oldMpin)
        goToEnterNewMenu
      case "E5108" =>
        session[Int]("login.retry.count").cata(retry, retry(0))
      case "E5000" =>
        #<>(ErrorCode.ERR501, "Internal error, no auth resp from common registration")
    }

  }
}