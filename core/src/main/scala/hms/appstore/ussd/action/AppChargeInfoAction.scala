/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.appstore.ussd.action

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s
import hms.appstore.ussd.util.SessionExtensions
import hms.appstore.api.json.Application

import scalaz._
import Scalaz._

class AppChargeInfoAction extends Action with Log4s {

  def execute(request: Request): Either[HandlerError, Result] = buildMessageString(request)

  private def buildMessageString(request: Request): Either[HandlerError, Result] = {
    import SessionExtensions._
    import request._
    session.getApp.cata(
      app => {
        val content = app.appTypes.mkString match {
          case "ondemand" => {
            val chargeInfo =  app.chargingDetails
            buildChargeInfo(appContext, app, chargeInfo)
          }
          case "subscription" => {
            val chargeInfo = app.chargingDetails
            buildChargeInfo(appContext, app, chargeInfo)
          }
          case "downloadable" => {
            val chargeInfo = app.chargingDetails
            buildChargeInfo(appContext, app, chargeInfo)
          }
          case _ => {
            buildChargeInfo(appContext, app, app.chargingDetails)
          }
        }
        #|>(content)
      },
      #<>(ErrorCode.ERR501, "Illegal state, missing page paramters")
    )
  }

  private def buildChargeInfo(ctx: AppContext, app: Application, chargeInfo: String): String = {
    List(app.displayName, "\n",
      chargeInfo, "\n",
      ctx.localize("back.menu.key.text"),
      ctx.localize("main.menu.key.text")).mkString
  }
}



