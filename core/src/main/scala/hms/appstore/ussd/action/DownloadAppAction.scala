package hms.appstore.ussd.action

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s
import hms.appstore.ussd.util.SessionExtensions

import scalaz._
import Scalaz._
import hms.appstore.ussd.service.ServiceModule
import hms.appstore.api.client.DiscoveryService
import scala.concurrent.ExecutionContext
import com.escalatesoft.subcut.inject.Injectable

class DownloadAppAction extends Action with Injectable with Log4s {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  def execute(request: Request): Either[HandlerError, Result] = {
    import SessionExtensions._
    import request._

    session.getApp.cata(
      app => {
        def buildMessageString(message: String) = {
          List(app.displayName, "\n",
            message, "\n",
            appContext.localize("back.menu.key.text"),
            appContext.localize("main.menu.key.text")).mkString
        }

        #|>(buildMessageString(appContext.localize("app.download.currently.not.supported")))
      },
      HandlerError(ErrorCode.ERR501, "Illegal state, missing page params").left
    )
  }
}