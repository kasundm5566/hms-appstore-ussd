package hms.appstore.ussd.action

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.menulet._
import hsenidmobile.iluka.iapi.utils.Log4s
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}

import scalaz._
import Scalaz._

import org.apache.commons.codec.digest.DigestUtils
import scala.beans.BeanProperty
import hms.appstore.api.client.DiscoveryInternalService
import scala.concurrent.{Await, ExecutionContext}
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.api.json.MPinChangeReq

class ConfirmChangeMpinAction extends Injectable with Validator with UssdConfig with Log4s {
  @BeanProperty var maxRetryAttempts = 3

  val bindingModule = ServiceModule
  private val discoveryInternalService = inject[DiscoveryInternalService]
  private implicit val executionContext = inject[ExecutionContext]

  override def execute(flowId: String, request: Request) = {
    import request._
    import msg.{data, msisdn}

    val existingFlowInputs = session.apply[Map[String, String]](flowId)

    def isSameMpin: Boolean = {
      try {
          val mpinEntered = session.apply[Map[String, String]]("changeNewMpin").getOrElse(Map.empty).getOrElse("entered-new-m-pin", "")

          if(mpinEntered === data) {
            info("MPin entered are matched")
            true
          }
          else {
            info("MPin entered are not matched")
            false
          }
      } catch {
        case e: Exception =>
          error("Exception occurred while retrieving 'entered-m-pin' from session attributes", e)
          false
      }
    }

    def retry(i: Int) = {
      if (i >= maxRetryAttempts) {
        #|(appContext.localize("mpin.confirm.retry.attempts.exceeded"))
      } else {
        session.+=("confirm.mpin.retry.count", i + 1)
        #>(nexActionName = "confirmChangeMpinRetryMenu")
      }
    }

    def goToSuccessMpinMenu: Either[HandlerError, Result] = {
      session.+=("confirm.mpin.retry.count", 0)

      val mpin = DigestUtils.md5Hex(data)
      val oldMpin = DigestUtils.md5Hex(session[String]("old.m.pin").getOrElse(""))

      val mpinChangeReq = MPinChangeReq(msisdn, mpin, Some(oldMpin))
      val changeMpinFuture = discoveryInternalService.changeMpin(mpinChangeReq)
      val changeMpinResp = Await.result(changeMpinFuture, discoveryApiTimeOut)

      if(changeMpinResp.statusCode.equals("S1000")) {
        existingFlowInputs |+| some(formInputAsMap(data)) map {session.+=(flowId, _)}
        #>(nexActionName = "successChangeMpinMenu")
      } else {
        #>(nexActionName = "index")
      }
    }

    if (isSameMpin) goToSuccessMpinMenu
    else session[Int]("confirm.mpin.retry.count").cata(retry, retry(0))
  }
}