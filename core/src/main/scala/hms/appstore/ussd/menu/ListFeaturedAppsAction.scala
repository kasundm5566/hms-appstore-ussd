package hms.appstore.ussd.menu

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.paging._
import hms.appstore.ussd.util.SessionExtensions._
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.json.ParentCategory

import scalaz._
import Scalaz._

import scala.concurrent.{Await, ExecutionContext}
import com.escalatesoft.subcut.inject.Injectable

class ListFeaturedAppsAction extends PagingAction with Injectable with ForwardResolver with UssdConfig {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  override val renderHeader: Renderer = viewContext => {
    import viewContext.request
    import request.{session, locale}

    session.getAppPanel.cata(
      panel => {
        ViewFactory.mkView(panel.name).right
      },
      HandlerError(ErrorCode.ERR501, "Illegal state, missing page params").left
    )

  }

  override val renderFooter: Renderer = viewContext => {
    import viewContext.request
    import request.{appContext, locale}

    val viewMsg = List(appContext.backKey + separator + "Back", appContext.homeKey + separator + "Home")

    ViewFactory.mkView(viewMsg.mkString("\n")).right
  }

  protected override def buildPage(viewCtx: DynamicViewContext, startIndex: Int, maxResults: Int): Page = {
    import viewCtx.request
    import request.session

    val pagingContext = session.getPagingCtx("index").get

    pagingContext.selectedOption[ParentCategory].fold(
      handlerError => handlerError |> errorPage
      ,
      panelOpt => {
        panelOpt.cata(
          panel => {
            session.setAppPanel(panel)

            val featuredAppsFuture = {
              panel.id match {
                case "mostly.used" =>  discoveryService.mostlyUsedApps(startIndex, maxResults)
                case "free.apps" => discoveryService.freeApps(startIndex, maxResults)
                case "newly.added" => discoveryService.newlyAddedApps(startIndex, maxResults)
                case "top.rated" => discoveryService.topRatedApps(startIndex, maxResults)
                case "featured.apps" => discoveryService.featuredApps(startIndex, maxResults)
              }
            }

            val FutureResult = Await.result(featuredAppsFuture, discoveryApiTimeOut)

            val featuredAppsList = for {
              app <- FutureResult.getResults if !app.downloadable
            } yield (app.displayName, "featuredAppMenu", Option(app))
            featuredAppsList |> newPage
          },
          HandlerError(ErrorCode.ERR501, "Illegal state, missing page params") |> errorPage
        )
      }
    )
  }
}