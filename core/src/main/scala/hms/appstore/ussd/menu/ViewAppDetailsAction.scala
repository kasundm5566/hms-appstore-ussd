package hms.appstore.ussd.menu

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.paging._
import hms.appstore.ussd.util.SessionExtensions._
import hms.appstore.api.json.Application

import scalaz._
import Scalaz._
import scala.math._
import scala.beans.BeanProperty

class ViewAppDetailsAction extends PagingAction with ForwardResolver {
  @BeanProperty var parentMenuName = "appListMenu"

  override val renderHeader: Renderer = viewContext => {
    import viewContext._
    import request._
    session.getApp.cata(
      app => {
        val buffer = new StringBuffer
        buffer.append(app.displayName).
          append(" ").
          append("*" * floor(app.rating).toInt).
          append(" \n").
          append("App By " + app.developer + "\n").
          append(app.shortDesc)
        ViewFactory.mkView(buffer.toString).right
      },
      HandlerError(ErrorCode.ERR501, "Illegal state, missing page params").left
    )
  }

  override protected def beforeExecute(viewCtx: DynamicViewContext) {
    import viewCtx.request
    import request.{session, msg}
    val pagingContext = session.getPagingCtx(parentMenuName).get

    pagingContext.selectedOption[Application].fold(
      handlerError => {},
      appOpt => {
        appOpt.cata(
        app => {
          session.setApp(app)
        }, {}
        )
      }
    )
  }
}