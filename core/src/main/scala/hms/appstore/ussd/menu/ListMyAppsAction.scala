package hms.appstore.ussd.menu

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.paging._
import hsenidmobile.iluka.iapi.utils.Log4s

import hms.appstore.ussd.util.SessionExtensions._
import hms.appstore.api.client.DiscoveryService
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}

import scalaz._
import Scalaz._
import scala.concurrent.Await
import com.escalatesoft.subcut.inject.Injectable

class ListMyAppsAction extends PagingAction with ForwardResolver with Injectable with UssdConfig with Log4s {
  val bindingModule = ServiceModule

  val discoveryService: DiscoveryService = inject[DiscoveryService]

  protected override def buildPage(viewCtx: DynamicViewContext, startIndex: Int, maxResults: Int): Page = {
    import viewCtx.request
    import request.session

    val authSessionId = session.getAuthSessionId.get

    val appResults = discoveryService.mySubscriptions(authSessionId)
    val apps = Await.result(appResults, discoveryApiTimeOut)

    val resultList = for {
      app <- apps.getResults
    } yield (app.displayName, "myAppMenu", Some(app))

    newPage(resultList)
  }
}