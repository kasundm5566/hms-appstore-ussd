package hms.appstore.ussd.menu

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.paging._
import hms.appstore.ussd.util.SessionExtensions._
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.json.Category

import scalaz._
import Scalaz._

import com.escalatesoft.subcut.inject.Injectable
import scala.concurrent.{Await, ExecutionContext}
import scala.beans.BeanProperty

class ListAppsAction extends PagingAction with Injectable with ForwardResolver with UssdConfig {
  @BeanProperty var parentMenuName = "appCategoriesMenu"

  val bindingModule = ServiceModule

  private val discoveryService = inject[DiscoveryService]

  private implicit val executionContext = inject[ExecutionContext]

  override val renderHeader: Renderer = viewContext => {
    import viewContext.request
    import request.{session, locale}

    session.getCategory.cata(
      category => ViewFactory.mkView(category.id).right,
      HandlerError(ErrorCode.ERR501, "Illegal state, missing page params").left
    )

  }

  override val renderFooter: Renderer = viewContext => {
    import viewContext.{request, pagingCtx}
    import request.{appContext, locale}

    val viewMsg = List(appContext.backKey + separator + "Back", appContext.homeKey + separator + "Home")

    ViewFactory.mkView(viewMsg.mkString("\n")).right
  }

  protected override def buildPage(viewCtx: DynamicViewContext, startIndex: Int, maxResults: Int): Page = {
    import viewCtx.request
    import request.session

    val pagingContext = session.getPagingCtx(parentMenuName).get

    pagingContext.selectedOption[Category].fold(
      handlerError => {
        handlerError |> errorPage
      },
      categoryOpt => {
        categoryOpt.cata(
          category => {
            session.setCategory(category)

            val appsByCategoryFuture = discoveryService.appsByCategory(category.id,startIndex,maxResults)

            val FutureResult = Await.result(appsByCategoryFuture, discoveryApiTimeOut)

            val appsByCategoryList = for {
              app <- FutureResult.getResults if !app.downloadable
            } yield (app.displayName, "appMenu", Option(app))
            appsByCategoryList |> newPage
          },
          HandlerError(ErrorCode.ERR501, "Illegal state, missing page params") |> errorPage
        )
      }
    )
  }
}