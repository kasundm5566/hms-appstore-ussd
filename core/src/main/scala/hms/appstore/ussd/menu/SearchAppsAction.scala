package hms.appstore.ussd.menu

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi.paging._
import hsenidmobile.iluka.iapi.{Request, ViewFactory}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.ussd.service.{ServiceModule, UssdConfig}

import scalaz._
import Scalaz._

import scala.concurrent.{Await, ExecutionContext}
import com.escalatesoft.subcut.inject.Injectable

class SearchAppsAction extends PagingAction with Injectable with ForwardResolver with UssdConfig{

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  override val renderFooter: Renderer = viewContext => {
    import viewContext.request
    import request.{appContext, locale}

    val viewMsg = List(appContext.backKey + separator + "Back", appContext.homeKey + separator + "Home")

    ViewFactory.mkView(viewMsg.mkString("\n")).right
  }

  protected override def buildPage(viewCtx: DynamicViewContext, startIndex: Int, maxResults: Int) : Page = {
    import viewCtx.request
    import request._
    import msg.data

    debug("Search for: " + data)
    val searchAppsFuture = discoveryService.searchApps(data, startIndex: Int, maxResults)
    val futureResult = Await.result(searchAppsFuture, discoveryApiTimeOut)

    val searchAppsList = for {
      searchApp <- futureResult.getResults if !searchApp.downloadable
    } yield (searchApp.displayName, "searchResultMenu", Some(searchApp))

    searchAppsList |> newPage
  }
}