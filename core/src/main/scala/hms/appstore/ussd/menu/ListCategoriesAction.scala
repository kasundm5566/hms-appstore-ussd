package hms.appstore.ussd.menu

import hsenidmobile.iluka.iapi.paging._
import hms.appstore.api.client.DiscoveryService
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}

import scalaz._
import Scalaz._

import scala.concurrent.{Await, ExecutionContext}
import com.escalatesoft.subcut.inject.Injectable
import hsenidmobile.iluka.iapi.ViewFactory

class ListCategoriesAction extends PagingAction with Injectable with ForwardResolver with UssdConfig {

  val bindingModule = ServiceModule

  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  override val renderFooter: Renderer = viewContext => {
    import viewContext.request
    import request.{appContext, locale}

    val viewMsg = List(appContext.backKey + separator + "Back", appContext.homeKey + separator + "Home")

    ViewFactory.mkView(viewMsg.mkString("\n")).right
  }

  protected override def buildPage(viewCtx: DynamicViewContext, startIndex: Int, maxResults: Int):Page = {

    val categoriesFuture = discoveryService.categoriesForPaging(startIndex, maxResults)
    val futureResult = Await.result(categoriesFuture, discoveryApiTimeOut)

    val categoryList = for {
      category <- futureResult.getResults
    } yield (category.id, "appListMenu", Some(category))

    categoryList |> newPage
  }
}