package hms.appstore.ussd.menu

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi.paging._
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}
import hms.appstore.api.client.DiscoveryService

import scalaz._
import Scalaz._

import scala.concurrent.{Await, ExecutionContext}
import com.escalatesoft.subcut.inject.Injectable

class MainMenuAction extends PagingAction with Injectable with ForwardResolver with UssdConfig {

  val bindingModule = ServiceModule

  private val discoveryService = inject[DiscoveryService]

  private implicit val executionContext = inject[ExecutionContext]

  protected override def buildPage(viewCtx: DynamicViewContext, startIndex: Int, maxResults: Int):Page = {

    val parentCategoriesFuture = discoveryService.parentCategories()

    val futureResult = Await.result(parentCategoriesFuture, discoveryApiTimeOut)

    val FutureList = for {
      parentCategory <- futureResult.getResults
    } yield (parentCategory.name, "featuredAppListMenu", Some(parentCategory))

    FutureList |> newPage

  }
}