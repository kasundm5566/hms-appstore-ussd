package hms.appstore.ussd.util

/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
 


object StatusCodes {
  val S1000 = "S1000"

  val E1010 = "E1010"
  val E1020 = "E1020"
  val E1030 = "E1030"
  val E1040 = "E1040"
  val E1050 = "E1050"
  val E1060 = "E1060"
  val E1070 = "E1070"

  val E5000 = "E5000"

}