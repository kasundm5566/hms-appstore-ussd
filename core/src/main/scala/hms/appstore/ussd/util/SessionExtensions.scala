package hms.appstore.ussd.util

/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

import hsenidmobile.iluka.iapi.Session
import hms.appstore.api.json.{ParentCategory, Category, Application}

class SessionExtensions(session: Session) {
  private val CATEGORY_KEY = "currentCategory"
  private val APP_PANEL_KEY = "currentAppPanel"
  private val APP_KEY = "currentApp"
  private val SEARCH_KEY = "currentSearchArg"
  private val SESSION_KEY = "currentSessionId"

  def setAuthSessionId(id: String) = session.+=(SESSION_KEY, id)

  def getAuthSessionId: Option[String] = session[String](SESSION_KEY)

  def setApp(app: Application) = session.+=(APP_KEY, app)

  def getApp: Option[Application] = session[Application](APP_KEY)

  def setCategory(category: Category) = session.+=(CATEGORY_KEY, category)

  def getCategory: Option[Category] = session[Category](CATEGORY_KEY)

  def setAppPanel(parentCategory: ParentCategory) = session.+=(APP_PANEL_KEY, parentCategory)

  def getAppPanel: Option[ParentCategory] = session[ParentCategory](APP_PANEL_KEY)

  def setSearchArg(arg: String) = session.+=(SEARCH_KEY, arg)

  def getSearchArg: Option[String] = session[String](SEARCH_KEY)

}

object SessionExtensions {
  implicit def sessionExtensions(session: Session) = new SessionExtensions(session)
}