package hms.appstore.ussd.util

/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

import java.io.{File, FileInputStream}
import java.util.Properties

object UssdUtil {

  val Safaricom1 = "25470([0-9]{7})".r
  val Safaricom2 = "25471([0-9]{7})".r
  val Safaricom3 = "25472([0-9]{7})".r
  val Airtel = "25473([0-9]{7})".r
  val Yu = "25475([0-9]{7})".r
  val Orange = "25477([0-9]{7})".r
  val Dialog = "9477([0-9]{7})".r
  val Vodafone = "679([0-9]{7})".r

  def getOperatorId(msisdn: String) = msisdn match {
    case Safaricom1(m) => "safaricom"
    case Safaricom2(m) => "safaricom"
    case Safaricom3(m) => "safaricom"
    case Airtel(m) => "airtel"
    case Yu(m) => "yu"
    case Orange(m) => "orange"
    case Dialog(m) => "dialog"
    case Vodafone(m) => "vodafone"
  }

}