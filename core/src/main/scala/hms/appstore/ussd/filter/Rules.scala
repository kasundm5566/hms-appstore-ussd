package hms.appstore.ussd.filter

import hsenidmobile.iluka.iapi._
import hsenidmobile.iluka.iapi.utils.Log4s
import hsenidmobile.iluka.iapi.paging._
import hms.appstore.ussd.util.SessionExtensions._

import scalaz._
import Scalaz._
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import hms.appstore.ussd.service.{UssdConfig, ServiceModule}
import hms.appstore.api.client.DiscoveryService
import scala.concurrent.Await
import hms.appstore.api.json.SubscriptionStatus

class AuthRule extends PageFilter with Log4s {
  def apply(request: Request) = {
    val authenticated = request.session.getAuthSessionId.isDefined
    debug("Auth rule [" + authenticated + "] for user [" + request.msg.msisdn + "]")
    authenticated
  }
}

class LoginRule extends PageFilter with Log4s {
  override def apply(request: Request) = {
    val needToLogin = request.session.getAuthSessionId.isEmpty
    debug("Login rule [" + needToLogin + "] for user [" + request.msg.msisdn + "]")
    needToLogin
  }
}

class SubExclusionRule extends PageFilter with Injectable with UssdConfig with Log4s {

  val bindingModule = ServiceModule

  val allowedStates = Set(SubscriptionStatus.UnRegistered)

  private val discoveryService = inject[DiscoveryService]

  def apply(request: Request) = {
    import request.session

    (session.getApp, session.getAuthSessionId) match {
      case (Some(app), Some(authSessionId)) => {
        val canSubscribe = if (app.subscription) {
          val appResultFuture = discoveryService.appDetails(authSessionId, app.id)
          val appResult = Await.result(appResultFuture, discoveryApiTimeOut)

          allowedStates.contains(appResult.getResult.subscriptionStatus)
        } else {
          false
        }
        debug("SubExclusion rule [" + canSubscribe + "] for user [" + request.msg.msisdn + "] appId [" + app.id + "]")
        canSubscribe
      }
      case _ => false
    }
  }
}

class UnSubExclusionRule extends PageFilter with Injectable with UssdConfig with Log4s {
  val bindingModule = ServiceModule

  val allowedStates = Set(SubscriptionStatus.Registered)

  private val discoveryService = inject[DiscoveryService]

  def apply(request: Request) = {
    import request.session

    (session.getApp, session.getAuthSessionId) match {
      case (Some(app), Some(authSessionId)) => {
        val canUnSubscribe = if (app.subscription) {
          val appResultFuture = discoveryService.appDetails(authSessionId, app.id)
          val appResult = Await.result(appResultFuture, discoveryApiTimeOut)

          allowedStates.contains(appResult.getResult.subscriptionStatus)
        } else {
          false
        }
        debug("UnSubExclusion rule [" + canUnSubscribe + "] for user [" + request.msg.msisdn + "] appId [" + app.id + "]")
        canUnSubscribe
      }
      case _ => false
    }
  }
}

class DownloadRule extends PageFilter with Log4s {
  def apply(request: Request) = {
    import request.session
    session.getApp.cata(app => {
      val canDownload = app.downloadable
      debug("Download rule [" + canDownload + "] for user [" + request.msg.msisdn + "] appId [" + app.id + "]")
      canDownload
    }, false)
  }
}