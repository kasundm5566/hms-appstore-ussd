DynamicMenu index {
    description="AppStore Main Menu"

    header = "@main.menu.header@"

    dynamicContentFirst = true

    filters = ["loginRedirect" = ["loginRule"], "myAppStoreMenu" = ["authRule"]]

    fixedContent=[ ["App Categories", "appCategoriesMenu"], ["Search Apps", "appSearchMenu"], ["Login", "loginRedirect"], ["My Appstore" , "myAppStoreMenu"]]

    pagingAction="mainPaging"

    Menu myAppStoreMenu {
        content="@my.appstore.content@"
        action=forward[1 = "myAppsListMenu", 2 = "changeMpinMenu"]

        DynamicMenu myAppsListMenu {
            header="@my.app.list.header@"
            pagingAction="listMyApps"

            DynamicMenu myAppMenu {
                header="@my.app.menu.header@"

                filters=["unSubscribe" = ["unSubscriptionRule"]]

                fixedContent=[["UnSubscribe", "unSubscribe"], ["Charges", "chargeInfo"], ["Information", "appInfo"]]

                pagingAction="viewMyAppDetails"
            }
        }
    }

    FormMenu changeMpinMenu {
        flowId="oldMpin"
        content="@change.mpin.enter.old.mpin@"
        validator="changeMpin"

        FormMenu validateRetryMenu {
            content="@login.retry.menu.content@"
            validator="changeMpin"
        }
    }

    Menu appSearchMenu {
        content= "@app.search.input.content@"
        action="searchResultsMenu"

        DynamicMenu searchResultsMenu {
            header="@search.results.menu.title@"
            pagingAction="searchApps"
        }
    }

    DynamicMenu appCategoriesMenu {
        header= "@category.apps.menu.title@"
        pagingAction="listCategories"
    }

    DynamicMenu appListMenu {
        header="@app.list.header@"
        pagingAction="listApps"
    }

    DynamicMenu featuredAppListMenu {
        header="@featured.app.list.header@"
        pagingAction="listFeaturedApps"
    }

    DynamicMenu searchResultMenu {
        header="@app.menu.header@"

        filters=["subscribe" = ["authRule","subscriptionRule"], "unSubscribe" = ["authRule", "unSubscriptionRule"], "download" = ["downloadRule"]]

        fixedContent=[["Subscribe", "subscribe"], ["UnSubscribe", "unSubscribe"], ["Download", "download"], ["Charges", "chargeInfo"], ["Information", "appInfo"]]

        pagingAction="viewSearchResult"
    }

    DynamicMenu appMenu {
        header="@app.menu.header@"

        filters=["subscribe" = ["authRule","subscriptionRule"], "unSubscribe" = ["authRule", "unSubscriptionRule"], "download" = ["downloadRule"]]

        fixedContent=[["Subscribe", "subscribe"], ["UnSubscribe", "unSubscribe"], ["Download", "download"], ["Charges", "chargeInfo"], ["Information", "appInfo"]]

        pagingAction="viewAppDetails"
    }

    DynamicMenu featuredAppMenu {
        header="@app.menu.header@"

        filters=["subscribe" = ["authRule","subscriptionRule"], "unSubscribe" = ["authRule", "unSubscriptionRule"], "download" = ["downloadRule"]]

        fixedContent=[["Subscribe", "subscribe"], ["UnSubscribe", "unSubscribe"], ["Download", "download"], ["Charges", "chargeInfo"], ["Information", "appInfo"]]

        pagingAction="viewFeaturedAppDetails"
    }

    FormMenu loginMenu {

        content="@login.menu.content@"
        validator="login"

        FormMenu loginRetryMenu {
            content="@login.retry.menu.content@"
            validator="login"
        }
    }

    FormMenu enterMpinMenu {

        flowId="newMpin"
        content="@login.mpin.enter@"
        validator="enterMpin"

        FormMenu enterMpinRetryMenu {

            flowId="newMpin"
            content="@login.mpin.enter.retry@"
            validator="enterMpin"
        }
    }

    FormMenu confirmMpinMenu {

        flowId="newMpin"
        content="@login.mpin.confirm@"
        validator="confirmMpin"

        FormMenu confirmMpinRetryMenu {

            flowId="newMpin"
            content="@login.mpin.confirm.retry@"
            validator="confirmMpin"
        }
    }

    FormMenu enterNewMpinMenu {

        flowId="changeNewMpin"
        content="@login.mpin.enter@"
        validator="enterNewMpin"

        FormMenu enterNewMpinRetryMenu {

            flowId="changeNewMpin"
            content="@login.mpin.enter.retry@"
            validator="enterNewMpin"
        }
    }

     FormMenu confirmChangeMpinMenu {

        flowId="changeNewMpin"
        content="@login.mpin.confirm@"
        validator="confirmChangeMpin"

        FormMenu confirmChangeMpinRetryMenu {

            flowId="changeNewMpin"
            content="@login.mpin.confirm.retry@"
            validator="confirmChangeMpin"
        }
    }

    Menu successChangeMpinMenu {

        content="@change.mpin.success@"
        action=forward[00 = "index"]
    }
    Menu successMpinMenu {

        content="@login.mpin.success@"
        action=forward[00 = "index"]
    }

    Menu notRegisteredMenu {

        content="@login.mpin.not.register@"
        action=forward[00 = "index"]
    }

    Menu downloadMenu {

        content="@app.download.choose.platform@"
        action=forward[00 = "index"]
    }

}
