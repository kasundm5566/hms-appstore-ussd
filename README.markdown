
What is appstore-ussd
----------------
appstore-ussd is a ussd channel for appstore

Pre Requirements
----------------
Java  JDK 1.6
Maven 3.0.3

Install Java and add JAVA_HOME/bin to system PATH variable
Install Maven and add M2_HOME/bin to system PATH variable

What is Core
----------------
Core is the deployment unit for appstore-ussd

Pre Requirements
----------------
1)install iluka v2.2.x
  refer <iluka.project.home>/README.txt

How to Build
------------
i.  checkout the relevant appstore-ussd
ii. set up the appstore db
iii.go to "appstore-ussd" and run "mvn clean install -Pdefault"

How to Deploy
-------------
1)from core/target, unzip the appstore-ussd-<version>.tar.gz and copy the folder appstore-ussd-<version> to <iluka.home>/apps directory

2)configure following connector properties and other porperties
  in <iluka.home>/apps/appstore-ussd-<version>/conf according to your deployment environment.
  eg: configure the following in conf/sdp-ussd-connector.properties
      #MO Setting
      ussd.listener.host=192.168.0.95
      ussd.listener.port=9090

      #MT Setting
      ussd.sender.host=192.168.0.95
      ussd.sender.port=7000

3)Add following DNS entry(please put relevant IP addresses)


127.0.0.1 core.appstore
127.0.0.1 db.mongo.appstore

How to test
-----------
Follow how-to-test documents in /docs according to your simulator



